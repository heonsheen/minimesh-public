//
// opengl_header.hpp
//

#ifndef MINIMESH_OPENGL_HEADERS_IS_INCLUDED
#define MINIMESH_OPENGL_HEADERS_IS_INCLUDED

#ifdef WIN32
#include <windows.h>
#undef near
#undef far
#endif

#ifdef __APPLE__
#  include <OpenGL/gl.h>
#  include <OpenGL/glu.h>
#  include <GLUT/glut.h>
#  include <GL/glui.h>
#else
#  include <GL/gl.h>
#  include <GL/glu.h>
#  include <GL/glut.h>
#  include <GL/glui.h>
#endif


#endif /* MINIMESH_OPENGL_HEADERS_IS_INCLUDED */
