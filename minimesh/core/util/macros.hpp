// Some handy macros
// Author: Shayan Hoshyari

#ifndef MINIMESH_MACROS_IS_INCLUDED
#define MINIMESH_MACROS_IS_INCLUDED

// Use this to get rid of unused variable warnings.
#define MINIMESH_UNUSED(x) (void)(x)

// You can use this macro as a shorthand for std::stringstream
// example: std::string name = MINIMESH_STR("run_number_" << i << ".vtk");
#define MINIMESH_STR(X) static_cast<std::ostringstream&>(std::ostringstream().flush() << X).str()

#endif /* MINIMESH_MACROS_IS_INCLUDED */
